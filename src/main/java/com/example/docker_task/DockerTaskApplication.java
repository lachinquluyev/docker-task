package com.example.docker_task;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DockerTaskApplication implements CommandLineRunner {

	public static void main(String[] args) {
		SpringApplication.run(DockerTaskApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {

	}
}
