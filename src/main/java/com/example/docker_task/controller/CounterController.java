package com.example.docker_task.controller;

import com.example.docker_task.entity.Counter;
import com.example.docker_task.repo.CounterRepository;
import com.example.docker_task.service.CounterService;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/hello")
@RequiredArgsConstructor
public class CounterController {
    private final CounterService counterService;

    @GetMapping()
    public String sayHello() {
        Counter counter = counterService.incrementCounter(1L);
        return "Hello, the counter is " + counter.getCount();
    }

}
