package com.example.docker_task.service;

import com.example.docker_task.entity.Counter;

public interface CounterService {
    Counter getCounterById(Long id);

    Counter incrementCounter(Long id);}
