package com.example.docker_task.service;

import com.example.docker_task.entity.Counter;
import com.example.docker_task.repo.CounterRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class CounterImpl implements CounterService{

    private final CounterRepository repository;
    @Override
    public Counter getCounterById(Long id) {
        return repository.findById(id).orElse(new Counter());
    }

    @Override
    public Counter incrementCounter(Long id) {
        Counter counter = getCounterById(id);
        counter.setCount(counter.getCount() + 1);
        return repository.save(counter);
    }
}
