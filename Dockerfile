FROM alpine:3.16.0
RUN apk --no-cache add openjdk17
COPY build/libs/Docker_Task-0.0.1-SNAPSHOT.jar /app/
WORKDIR /app/
ENTRYPOINT ["java"]
CMD ["-jar", "/app/Docker_Task-0.0.1-SNAPSHOT.jar"]